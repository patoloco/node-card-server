"use strict";

const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const DbConfig = require("./config/db.js");
const routes = require("./routes.js");

const port = process.env.PORT || 3000;
mongoose.Promise = global.Promise;

// DbConfig.localConnectionString for local DB
mongoose.connect(DbConfig.remoteConnectionString)
   .then(() => {
     console.log("Connected to the DB");
     startExpress();
   })
   .catch((err) => {
     console.error(err.message);
     process.exit(1);
   });

function startExpress () {
  const app = express();

  console.log("Starting Express on port " + port);

  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  routes(app);

  app.listen(port);
}
