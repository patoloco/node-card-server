const Cards = require("./models/cardModel");

module.exports = function (app) {
  app.get("/card/", function (req, res, next) {
    res.send("Must specify a card name");
  });

  app.get("/card/:name", function (req, res, next) {
    const cardname = req.params.name;
    const exp = new RegExp("^" + cardname + "$", "i");

    Cards.findOne({ name: exp }).exec()
      .then((card) => {
        if (card) {
          res.send(card);
        } else {
          res.send("Card not found");
        }
      })
      .catch((err) => {
        console.error("DB Error: ", err);
        res.send("DB Error");
      });
  });

  app.get("/search/", function (req, res, next) {
    res.send([]);
  });

  app.get("/search/:string", function (req, res, next) {
    const string = req.params.string;
    const cardlist = [];

    const exp = new RegExp(string, "i");

    Cards.find({ "name": exp }).exec()
      .then((cards) => {
        for (let card of cards) {
          cardlist.push(card.name);
        }
        res.send(cardlist);
      })
      .catch((err) => {
        console.error("DB Error: ", err);
        res.send("DB Error");
      });
  });

  app.get("/searchtext/", function(req, res, next) {
    res.send("Must specify string to search for");
  });

  app.get("/searchtext/:string", function (req, res, next) {
    const text = req.params.string;
    const textlist = [];

    Cards.find({ "text" : { $regex: text} }).exec()
      .then((cards) => {
        for (let card of cards) {
          textlist.push({"name": card.name, "text": card.text});
        }
        if (textlist) {
          res.send(textlist);
        } else {
          res.send("No cards found");
        }
      })
      .catch((err) => {
        console.error("DB Error: ", err);
        res.send("DB Error");
      });
  });

  app.get("*", function (req, res) {
    res.send("Try /card/:name or /search/:name or /searchtext/:string");
  });

};
