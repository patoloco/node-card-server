const localuser = {
  uname: "tester",
  pwd: "testthis",
  DB: "tekkis"
};

const user = {
  uname: process.env.uname,
  pwd: process.env.pwd,
  DB: "tekkis"
};

module.exports = {
  localConnectionString: `mongodb://${localuser.uname}:${localuser.pwd}@localhost:27017/${localuser.DB}`,
  remoteConnectionString: "mongodb://" + user.uname + ":" + user.pwd + "@mustard-shard-00-00-ij0x5.mongodb.net:27017,mustard-shard-00-01-ij0x5.mongodb.net:27017,mustard-shard-00-02-ij0x5.mongodb.net:27017/" + user.DB + "?ssl=true&replicaSet=Mustard-shard-0&authSource=admin"
};
