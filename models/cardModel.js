const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cardSchema = new Schema({
  layout: String,
  name: String,
  manaCost: String,
  cmc: Number,
  colors: [String],
  type: String,
  types: [String],
  subtypes: [String],
  text: String,
  power: String,
  toughness: String,
  imageName: String,
  colorIdentity: [String]
});

cardSchema.index({
  text: "text"
});

module.exports = mongoose.model("Cards", cardSchema);
