const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const deckSchema = new Schema({
  name: String,
  variation: Number,
  maindeck: [{type: Schema.Types.ObjectId, ref: "Cards"}],
  sideboard: [{type: Schema.Types.ObjectId, ref: "Cards"}],
  bench: [{type: Schema.Types.ObjectId, ref: "Cards"}]
});

deckSchema.index({
  name: "text"
});

module.exports = mongoose.model("Decks", deckSchema);
